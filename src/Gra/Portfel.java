package Gra;

public class Portfel {


    int stanKonta =1000;


    public Portfel(int stanKonta) {
        this.stanKonta = stanKonta;
    }


    public int getStanKonta() {
        return stanKonta;
    }
    public void setStanKonta(int stanKonta) {
        this.stanKonta = stanKonta;
    }

    public int prawidlowaOdpowiedz(){
       return stanKonta +=100;
    }

    public int blednaOdpowiedz(){
       return stanKonta -=10;
    }

    public void wyswietlStanKonta(){
        System.out.println("Twój obecny stan konta to: "+stanKonta);
    }
}
