package Gra;

import java.util.Random;
import java.util.Scanner;

public class Questions2 {
    static final int QUESTION = 0;
    static final int ANSWER = 1;
    static final int START_OF_CHOICES = 2;

    //init questions, answers, and choices
    static String[][] multiChoice = new String[][]{
            {"1. Podaj stolicę Polski ?", "B", "A. Kraków", "B. Warszawa", "C. Gdańsk", "D. Wrocław"},
            {"2. Podaj stolicę Stanów Zjednoczonych ?", "C", "A. New York", "B. San Francisco", "C. Washington", "D. San Diego"},
            {"3. Podaj stolicę Kanady ?", "A", "A. Ottawa", "B. Toronto", "C. Montreal", "D. Vancouver"},
            {"4. Podaj stolicę Francji ?", "A", "A. Paryż", "B. Bordeaux", "C. Marsylia", "D. Nantes"},
            {"5. Podaj stolicę Niemiec ?", "B", "A. Frankfurt", "B. Berlin", "C. Dortmund", "D. Monachium"},
            {"6. Podaj stolicę Wielkiej Brytanii ?", "C", "A. Birmingham", "B. Leicester", "C. Londyn", "D. Manchester"},
            {"7. Podaj stolicę Hiszpanii ?", "C", "A. Barcelona", "B. Valencia", "C. Madryt", "D. Malaga"},
            {"8. Podaj stolicę Indii ?", "C", "A. Bombaj", "B. Bangalore", "C. New Delhi", "D. Ahmadabad"},
            {"9. Podaj stolicę Australii ?", "B", "A. Sydney", "B. Canberra", "C. Melbourne", "D. Brisbane"},
            {"10. Podaj stolicę Brazylii ?", "D", "A. Salwador", "B. Rio de Janeiro", "C. Sao Paulo", "D. Brasilia"},
            {"11. Podaj stolicę Norwegii ?", "D", "A. Lillehammer", "B. Trondheim", "C. Bergen", "D. Oslo"},
            {"12. Podaj stolicę Szwecji ?", "D", "A. Falun", "B. Karlstad", "C. Goteborg", "D. Sztokholm"},
            {"13. Podaj stolicę Maroko ?", "D", "A. Marakesz", "B. Fez", "C. Agadir", "D. Rabat"},
            {"14. Podaj stolicę Portugalii ?", "B", "A. Braga", "B. Lizbona", "C. Porto", "D. Coimbra"},
            {"15. Podaj stolicę Austrii ?", "D", "A. Innsbruck", "B. Graz", "C. Salzburg", "D. Wiedeń"},
            {"16. Podaj stolicę Szwajcarii?", "C", "A. Berno", "B. Lucerna", "C. Zurych", "D. Genewa"},
            {"17. Podaj stolicę Argentyny ?", "D", "A. Puerto Santa Cruz", "B. San Juan", "C. Cordoba", "D. Buenos Aires"},
            {"18. Podaj stolicę Rosji ?", "A", "A. Moskwa", "B. Petersburg", "C. Kaliningrad", "D. Smoleńsk"},
            {"19. Podaj stolicę Japonii ?", "D", "A. Hiroszima", "B. Osaka", "C. Sapporo", "D. Tokio"},
            {"20. Podaj stolicę Włoch ?", "B", "A. Piza", "B. Rzym", "C. Mediolan", "D. Juventus"}};


    //method that checks whether the user's answer is correct for a particular question
    public static boolean isCorrectAnswer(int questionNum, char userAnswer) {
        //true if matched, false otherwise
        boolean rightAnswer = (userAnswer + "").equalsIgnoreCase(multiChoice[questionNum][ANSWER]);
        //equivalent to if rightAnswer is true then display "Correct", else, display "Incorrect"
        System.out.println(rightAnswer ? "\nDobra odpowiedź!\notrzymujesz 100PLN\n" :
                "\nZła odpowiedź, spróbuj raz jeszcze!\nwłaśnie straciłeś 10PLN\n");

        return rightAnswer;
    }



    //method that prints a specific question and its choices
    static void printQuestion(int questionNum) {
        System.out.println(multiChoice[questionNum][QUESTION]);
        int lastColumn = multiChoice[questionNum].length;
        for (int x = START_OF_CHOICES; x < lastColumn; x++) {
            System.out.println("\t" + multiChoice[questionNum][x]);
        }

    }
}
